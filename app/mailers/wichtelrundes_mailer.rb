class WichtelrundesMailer < ApplicationMailer
  def wichtel(user, wichtelrunde, uuid)
    @user = user
    @wichtelrunde = wichtelrunde
    @uuid = uuid

    mail(to: @user.email)
  end
end
