class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
    @current_wichtelrunden = Wichtelrunde.where("register_until >= ? OR ship_until >= ?", DateTime.now, DateTime.now).order(register_until: :desc)
    @past_wichtelrunden = Wichtelrunde.where("register_until < ? AND ship_until < ?", DateTime.now, DateTime.now).order(register_until: :asc)
  end

  def invite
    User.invite! invite_params

    flash[:notice] = t(".success")

    redirect_to root_path
  end

  private
    def invite_params
      params.permit(:email)
    end
end
