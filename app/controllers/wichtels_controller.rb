class WichtelsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_wichtelrunde, only: [:create, :update, :destroy]
  before_action :set_wichtel, only: [:update, :destroy]
  before_action :can_register, only: [:create, :update, :destroy]

  def create
    @wichtel = Wichtel.new wichtelrunde_params
    @wichtel.user = current_user
    @wichtel.wichtelrunde = @wichtelrunde
    @wichtel.save!

    flash[:notice] = t(".created")

    redirect_to @wichtelrunde
  end

  def update
    @wichtel.update wichtelrunde_params

    flash[:notice] = t(".updated")

    redirect_to @wichtelrunde
  end

  def destroy
    @wichtel.destroy

    flash[:notice] = t(".destroyed", register_until: l(@wichtelrunde.register_until))

    redirect_to @wichtelrunde
  end

  private
    def can_register
      if @wichtelrunde.registration_over
        flash[:alert] = t(".register_until_over")
        redirect_to @wichtelrunde
      end
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_wichtelrunde
      @wichtelrunde = Wichtelrunde.find(params[:wichtelrunde_id])
    end

    def set_wichtel
      @wichtel = current_user.wichtels.where(id: params[:id], wichtelrunde: @wichtelrunde).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wichtelrunde_params
      params.require(:wichtel).permit(:shipping_address)
    end

end
