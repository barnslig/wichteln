class WichtelrundesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_wichtelrunde, only: [:show, :sender_show, :sender_update]
  before_action :set_sender_wichtel, only: [:sender_show, :sender_update]

  def show
    @wichtel = current_user.wichtels.where(wichtelrunde: @wichtelrunde).first
    unless @wichtel
      @wichtel = Wichtel.new
    end
  end

  def sender_show
  end

  def sender_update
    @wichtel.update sender_wichtel_params

    flash[:notice] = t(".updated")

    redirect_to sender_wichtelrunde_path(@wichtelrunde, @wichtel.lot_uuid)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_wichtelrunde
      @wichtelrunde = Wichtelrunde.find(params[:id])
    end

    def set_sender_wichtel
      @wichtel = @wichtelrunde.wichtels.where(lot_uuid: params[:uuid]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def wichtelrunde_params
      params.fetch(:wichtelrunde, {})
    end

    def sender_wichtel_params
      params.require(:wichtel).permit(:shipped)
    end
end
