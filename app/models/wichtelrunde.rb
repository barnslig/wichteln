class Wichtelrunde < ApplicationRecord
  has_many :wichtels
  has_many :users, through: :wichtels

  scope :should_draw_lots, -> {
    where("register_until < ? AND ship_until >= ?", DateTime.now, DateTime.now)
  }

  def registration_over
    self.register_until < DateTime.now
  end
end
