require 'test_helper'

class WichtelrundesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @wichtelrunde = wichtelrundes(:one)
  end

  test "should get index" do
    get wichtelrundes_url
    assert_response :success
  end

  test "should get new" do
    get new_wichtelrunde_url
    assert_response :success
  end

  test "should create wichtelrunde" do
    assert_difference('Wichtelrunde.count') do
      post wichtelrundes_url, params: { wichtelrunde: {  } }
    end

    assert_redirected_to wichtelrunde_url(Wichtelrunde.last)
  end

  test "should show wichtelrunde" do
    get wichtelrunde_url(@wichtelrunde)
    assert_response :success
  end

  test "should get edit" do
    get edit_wichtelrunde_url(@wichtelrunde)
    assert_response :success
  end

  test "should update wichtelrunde" do
    patch wichtelrunde_url(@wichtelrunde), params: { wichtelrunde: {  } }
    assert_redirected_to wichtelrunde_url(@wichtelrunde)
  end

  test "should destroy wichtelrunde" do
    assert_difference('Wichtelrunde.count', -1) do
      delete wichtelrunde_url(@wichtelrunde)
    end

    assert_redirected_to wichtelrundes_url
  end
end
