Rails.application.routes.draw do
  devise_for :users
  as :user do
    get 'users/edit' => 'devise_invitable/registrations#edit', :as => 'edit_user_registration'
    put 'users' => 'devise_invitable/registrations#update', :as => 'user_registration'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: "home#index"

  post '/invite', to: 'home#invite'

  resources :wichtelrundes, only: [:show] do
    resources :wichtels, only: [:create, :update, :destroy]

    member do
      get ':uuid', action: :sender_show, as: :sender
      patch ':uuid', action: :sender_update
      put ':uuid', action: :sender_update
    end
  end
end
