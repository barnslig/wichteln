namespace :wichtelrunde do
  desc "Draw lots and send them to random participants for Wichtelrunden where the registration period has finished"
  task :send_lots => [:environment] do

    Wichtelrunde.should_draw_lots.each do |wichtelrunde|
      wichtelrunde.wichtels.each do |wichtel|
        next if wichtel.lot_drawn

        # Set the lot uuid
        wichtel.lot_uuid = SecureRandom.uuid

        # get another user from this wichtelrunde which has no lot
        luckyone = wichtelrunde.wichtels.where.not(id: wichtel.id).where(has_lot: [nil, false]).sample

        # shit an odd number of participants ._.
        unless luckyone
          puts "fuck"
        end

        # Send an e-mail
        WichtelrundesMailer.wichtel(luckyone.user, wichtelrunde, wichtel.lot_uuid).deliver_now

        # Save that both 
        wichtel.lot_drawn = true
        wichtel.save!

        luckyone.has_lot = true
        luckyone.save!
      end
    end

  end
end