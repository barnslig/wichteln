class CreateWichtels < ActiveRecord::Migration[5.0]
  def change
    create_table :wichtels do |t|
      t.belongs_to :user, index: true
      t.belongs_to :wichtelrunde, index: true

      t.string :shipping_address
      t.string :lot_uuid
      t.boolean :lot_drawn

      t.timestamps
    end
  end
end
