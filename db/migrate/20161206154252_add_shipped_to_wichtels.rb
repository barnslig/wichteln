class AddShippedToWichtels < ActiveRecord::Migration[5.0]
  def change
    add_column :wichtels, :shipped, :boolean
  end
end
