class ChangeWichtelrundeRulesToText < ActiveRecord::Migration[5.0]
  def change
    change_column :wichtelrundes, :rules, :text
  end
end
