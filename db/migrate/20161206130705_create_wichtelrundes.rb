class CreateWichtelrundes < ActiveRecord::Migration[5.0]
  def change
    create_table :wichtelrundes do |t|
      t.string :name
      t.string :rules
      t.datetime :register_until
      t.datetime :ship_until

      t.timestamps
    end
  end
end
