class AddHasLotToWichtels < ActiveRecord::Migration[5.0]
  def change
    add_column :wichtels, :has_lot, :boolean
  end
end
